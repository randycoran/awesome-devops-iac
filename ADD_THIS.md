

https://pulpproject.org/
https://www.sonatype.com/product-nexus-repository
https://cloud.google.com/artifact-registry
https://aws.amazon.com/about-aws/whats-new/2020/06/introducing-aws-codeartifact-a-fully-managed-software-artifact-repository-service/
https://skaffold.dev/
https://github.com/kubernetes-sigs/kustomize
https://stackstorm.com/
https://argoproj.github.io/argo-events/
https://knative.dev/
https://authy.com/blog/authy-vs-google-authenticator/
