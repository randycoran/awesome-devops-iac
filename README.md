# Awesome DevOps / Infrastructure as code

Programs and applications that help with building infrastructure as code.

Following in the footsteps of [Awesome-sysadmin](https://github.com/kahun/awesome-sysadmin) and [Awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)

### code cleaning

* [BFG](https://rtyley.github.io/bfg-repo-cleaner/)

### Dashboards

* [Dashy](https://dashy.to/)

### Logging / monitoring

* [Prometheus]()
* [Cortex](https://github.com/cortexproject/cortex)
* ([grafana]()
* [Grafana-Loki](https://github.com/grafana/loki)

### Wiki / documentation

* [requarks wiki](https://github.com/Requarks/wiki)

### Drawing / Diagrams
* [draw.io](https://github.com/jgraph/drawio)

### Storeage / Backup
* [remote storage](https://remotestorage.io/)

### Containers / Kubernetes

* [matchbox](https://github.com/poseidon/matchbox)
* [typhoon](https://github.com/poseidon/typhoon)
* [k8s-GPU-guide](https://github.com/Langhalsdino/Kubernetes-GPU-Guide)
